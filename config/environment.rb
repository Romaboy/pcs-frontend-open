# Load the Rails application.
require File.expand_path('../application', __FILE__)
BACKEND_API_CONFIG = YAML.load_file("#{Rails.root}/config/backend_api.yml")
# Initialize the Rails application.
Rails.application.initialize!
