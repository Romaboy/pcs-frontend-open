class SessionsController < ApplicationController
  def new
  end

  def create
    user = get_user_from_api(params[:user]['email'], params[:user]['password'])
    if user then
      session[:user_email] = user['email']
      session[:password] = user['pwd']
      redirect_to :list_orders, :notice => "Logged in!"
    else 
      flash.now.alert = "Invalid email or password"
      render "new"
    end
  end

  def destroy
    session[:user_email] = nil
    session[:password] = nil
    redirect_to :log_in, :notice => "Logged out!"
  end
end