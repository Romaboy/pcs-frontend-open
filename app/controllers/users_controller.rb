class UsersController < ApplicationController
  self.hbw_available = true

  PARAMS_ATTRIBUTES = [:name, :last_name, :middle_name, :company, :department,
                       :email, :role, :password, :password_confirmation]

  before_action :authenticate_user!
  #before_action :admin_only, except: [:show, :lookup]
  #before_action :find_user, except: [:new, :add, :index, :lookup]

  def index
    @users = getUserList
  end

  def show
    @user = JSON.parse(RestClient.get( BACKEND_API_CONFIG['url'] + "user/get/#{params[:id]}.com" ))
  end

  def update
    attributes = secure_params
    if attributes[:password].blank?
      attributes.delete(:password)
      attributes.delete(:password_confirmation)
    end
    if @user.update_attributes(attributes)
      redirect_to users_path, notice: t('user_updated')
    else
      render action: :edit
    end
  end

  def destroy
    user.destroy
    redirect_to users_path, notice: t('user_deleted')
  end

  def edit
  end
  def log_in
    redirect_to 'google.com'#controller: :orders,  action: :index
  end
  def new
    @user = User.new
  end

  def add
    post_params = {};
    if params['json_definition'].blank?
      temp = secure_params

      post_params['email'] = temp['email']
      post_params['pwd'] = temp['password']
      post_params['name'] = temp['name']
      post_params['last_name'] = temp['last_name']
      post_params['role'] = temp['role']
      post_params['company'] = temp['company']
      post_params['department'] = temp['department']
      post_params['status'] = 1
      @test = post_params
    else
      post_params = params['json_definition']
    end
    begin
      @res = RestClient.post BACKEND_API_CONFIG['url'] + "user/adduser", post_params.to_json, :content_type => :json, :accept => :json
      @req = @res.request
    rescue => e
      @error = e.response.to_str
    end
    if !@res.nil? && @res.code == 200
      flash = {success: 'Success!'}
    else
      flash = {error: @error + post_params.to_json}
    end
    redirect_to users_path, flash: flash
  end

  def generate_api_token
    @user.generate_api_token
    render action: 'show'
  end

  def clear_api_token
    @user.clear_api_token
    render action: 'show'
  end

  def lookup
    a = []
    users = []
    getUserList.each_with_index{|u, i|
      a.push ( {"id" => u['email'], "text" => u['email']} )
    }
    a.each do |u|
      users.push(u) if u['text'].include?(params[:q])
    end
    render json: users
  end

  private

  def find_user
    @user = User.find(params[:id])
  end

  def admin_only
    redirect_to :back, alert: 'Access denied.' unless current_user.admin?
  end

  def secure_params
    params.require(:user).permit(*PARAMS_ATTRIBUTES)
  end
end
