class Admin::OrderTypesController < ApplicationController
  before_action :authenticate_user!
  self.hbw_available = true

  before_filter :require_order_type, only: [:show, :activate, :dismiss, :destroy]

  def index
    # @order_types = OrderType.active

    @order_types = []
    JSON.parse(RestClient.get(BACKEND_API_CONFIG['url'] + 'forms/types')).each_pair do |key, value|
      @order_types.push(name: value, code: key)
    end
    
    
  end

  def show
  end

  def update
    data = YAML::load(YAML::dump(params['order_type_form']['yaml']))    
    url = BACKEND_API_CONFIG['url'] + "forms/addOrderType" 
    begin
      @res = RestClient.post(url, data, 'Content-Type' => 'application/json')
    rescue => e
      @error = e.response.to_str
    end
    params[:id] = params[:order_type_id]

    respond_to do |format|
      format.html { redirect_to admin_order_type_path(params[:id]) }
      format.js do
        if !@res.nil? && @res.code == 200
          render json: 'Successfully updated!', status: 200
        else
          render json: @error.to_json, status: 402
        end
      end
    end
  end

  def create
    require 'yaml'
    file_content = params.require(:order_type).permit(:file)[:file].read
    @order_type = OrderType.new(file: file_content)
    @file = File.open(params[:order_type][:file].path())
      url = BACKEND_API_CONFIG['url'] + "forms/upload"
      request = RestClient::Request.new(
          :method => :post,
          :url => url,
          :enctype => "multipart/form-data",
          :payload => {
            :multipart => true,
            :file => @file
          })      

      begin
        # @res = RestClient.post(url, @file, 'Content-Type' => 'multipart/form-data', 'ENCTYPE' => "multipart/form-data", "Accept-Encoding" => "gzip, deflate")
        @res = request.execute
        @error = @res
      rescue => e
        @error = e.response.to_str
      end
      
      if !@res.nil? == 200 && @res.code
        render action: 'show'
      else
        @order_types = []
        render 'index'
        # redirect_to action: 'index' 
      end

    # if @order_type.valid?
    #   # 
            

    #   # @res = RestClient.post, file_content, :content_type => :json, :accept => :json
      
    #   # if @order_type.save_if_differs!
    #   #   flash[:success] = t('.uploaded')
    #   #   render action: 'show'
    #   # else
    #   #   flash[:error] = '%s: %s' % [t('.equal_exists'), @order_type.code]
    #   #   redirect_to action: 'index'
    #   # end
    # else
    #   flash[:error] = @order_type.errors.full_messages.join(', ')
    #   redirect_to action: 'index'
    # end
  end

  def destroy
    # @order_type.make_invisile
    @url = BACKEND_API_CONFIG['url'] + "forms/#{@order_type[:code]}"
    
    begin
      @res = RestClient.delete(@url)
      flash[:success] = t('.destroyed')
      redirect_to admin_order_types_path
    rescue => e
      @error = e.response.to_str + "<br>DETELE METHOD"
      @order_types = []
      render 'index'
    end
    
  end

  def activate
    @order_type.activate

    flash[:success] = t('.activated')
    redirect_to action: 'index'
  end

  def dismiss
    @order_type.destroy

    flash[:success] = t('.dismissed')
    redirect_to action: 'index'
  end

  def lookup
    render json: OrderType.lookup(params.require(:q)).to_json
  end

  private

  def require_order_type
    params[:id] = URI.decode(params.require(:id)).gsub('{dot}', '.')
    url = BACKEND_API_CONFIG['url'] + 'forms/json/' + params[:id]
    order_info = JSON.parse(RestClient.get(url))
    order_name = order_info['formName']
    order_code = order_info['code']
    url = BACKEND_API_CONFIG['url'] + 'forms/yaml/' + params[:id]
    yaml = RestClient.get(url)

    @order_type = {}
    @order_type[:name] = order_name
    @order_type[:code] = order_code
    @order_type[:active] = true
    @order_type[:yaml] = yaml
    @order_type[:html] = CodeRay.scan(yaml, :yml).div(
      tab_width: 0,
      css: :style,
      line_numbers: nil
    )

  end
end
