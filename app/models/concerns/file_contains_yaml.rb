module FileContainsYaml
  extend ActiveSupport::Concern

  def hash_from_file
    return @hash if @hash
    #yml = "common: &common\n label: ''\n type: string\n\norder_type:\n code: vacation_request\n name: Vacation Request\n fields:\n employeeFirstName:\n <<: *common\n label: First Name\n employeeLastName:\n <<: *common\n label: Last Name\n employeeEmail:\n <<: *common\n label: E-mail\n beginDate:\n <<: *common\n label: Begin Date\n type: datetime\n endDate:\n <<: *common\n label: End Date\n type: datetime\n motivationText:\n <<: *common\n label: Motivation Text\n resolution:\n <<: *common\n label: Resolution\n resolutionText:\n <<: *common\n label: Resolution Text\n adjustResult:\n <<: *common\n label: Adjust Result"
    @hash = YAML.load(file)
    @hash.is_a?(Hash) ? @hash.deep_symbolize_keys! : @hash = { }
  rescue Psych::SyntaxError
    logger.warn("Warning: #{__FILE__}:#{__LINE__} rescued \
                Psych::SyntaxError while parsing file: #{file[0..127]}")
    @hash = { }
  end

  def html_from_file
    a = file.dup
    dd
    content = file.dup.force_encoding(Encoding::UTF_8)

    CodeRay.scan(content, :yml).div(
      tab_width: 0,
      css: :style,
      line_numbers: nil
    )
  end
end
