$(document).ready( function (){
  /* body... */
  a = 0
  $("#new_order_form").on("ajax:complete",function (e, data, status, xhr) {
    /* body... */
    $('.hbw-button').attr('disabled',false)
    a += 1
    responce = JSON.parse(JSON.parse(data.responseText));
    console.log(responce);
    message = ''
    message += 'Task completed at::' + getISODateTime(responce['processStart']) + "\n"
    //message += 'Process Instance ID:' + responce['processInstanceId'] + "\n"
    message += 'Process Version: ' + responce['processName'] + " " + responce['businessVersionInfo'] + "\n"
    message += 'Process state is: ' + responce['processBusinessState'] + "\n"
    console.log('Submitted' + a);
    Application.messenger.show([["notice", message]])
  })
})

function getISODateTime(d){
    // padding function
    var s = function(a,b){return(1e15+a+"").slice(-b)};
    d=new Date(d);
    // return ISO datetime
    return d.getFullYear() + '-' +
        s(d.getMonth()+1,2) + '-' +
        s(d.getDate(),2) + ' ' +
        s(d.getHours(),2) + ':' +
        s(d.getMinutes(),2) + ':' +
        s(d.getSeconds(),2);
}
